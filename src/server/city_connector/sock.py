'''
Created on 05-11-2012

@author: gregor
'''
import socket, struct, sys
from server.city_connector.model import Map

GET_MAP = 0
BUILD_HOUSE = 1
BUILD_RAIL = 2
END_CONN = 3

class SocketConnector(object):
	def __init__(self, host, port):
		try:
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.connect((host, port))
			self.game = MySocket(s)
		except socket.error, msg:
			print '\nFailed to connect to the game socket. \n' + msg[1] + " (" + str(msg[0]) + ")"
			print "Tried to connect to " + host + ":" + str(port), "\n"
			sys.exit();
	
	def get_map(self):
		self.game.write32(GET_MAP)
		self.game.read32()
		width = self.game.read32()
		height = self.game.read32()
		m = Map(width, height)
		for row in m.rows:
			for field in row:
				field.height = self.game.read8()
				field.tileType = self.game.read8()
				field.zoneType = self.game.read8()
		return m
		pass
	
	def build(self,point,tile_type):
		index = point.y * self.get_map().width + point.x
		self.game.write32(BUILD_HOUSE)
		self.game.write32(index)
		self.game.write32(tile_type)
		return self.game.read32() == 1

# implement low level data packaging
class MySocket(object):
	def __init__(self, s):
		self.s = s
	
	def write8(self, data):
		self.s.sendall(struct.pack("B", data));
	
	def write32(self, data):
		self.s.sendall(struct.pack("I", data));
	
	def read8(self):
		return struct.unpack("B", self.s.recv(1))[0];
	
	def read32(self):
		return struct.unpack("I", self.s.recv(4))[0];
