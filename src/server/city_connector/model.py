'''
Created on 05-11-2012

@author: gregor
'''

class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

class MapField(object):
    def __init__(self, point):
        self.point = point
        self.height = 0
        self.tileType = 0
        self.zoneType = 0       

class Map(object):
    def __init__(self,width,height):
        self.width = width
        self.height = height
        # initialize field by constructor so they know where they are
        self.rows = [[MapField(Point(x, y)) for x in range(width)] for y in range(height)]
