'''
Module with model using to define web service
'''

from spyne.model.complex import ComplexModel, Array
from spyne.model.enum import Enum
from spyne.model.primitive import Integer

zone_types = ["None", "Residential", "Commercial", "Industrial"]

tile_types = ["CLEAR", "RAILWAY", "ROAD", "HOUSE", "TREES", \
				"STATION", "WATER", "VOID", "INDUSTRY", \
				"TUNNELBRIDGE", "OBJECT"]

# building names for the SOAP
building_types = ["BlockRed", "Block", "SmallHouse", "BlockGrey", "BlockBig", "VillageHouse", "Observatory", "Hole"]

# corresponding building ids from OpenTDD
building_ids = [0, 1, 2, 3, 4, 44, 38, 48]

ZoneType = Enum(*zone_types, type_name="ZoneType")

TileType = Enum(*tile_types, type_name="TileType")

BuildingType = Enum(*building_types, type_name="BuildingType")

class Point(ComplexModel):
    __namespace__ = 'open-city.soap'

    x = Integer
    y = Integer

class MapField(ComplexModel):
    __namespace__ = 'open-city.soap'
    
    point = Point
    height = Integer
    tile_type = TileType
    zone_type = ZoneType
    
class MapFieldRow(ComplexModel):
    __namespace__ = 'open-city.soap'
    
    fields = Array(MapField)
    
class Map(ComplexModel):
    __namespace__ = 'open-city.soap'
    
    height = Integer
    width = Integer
    rows = Array(MapFieldRow)
    
