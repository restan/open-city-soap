'''
Module with client gui
'''

from PySide import QtGui

class AppWidget(QtGui.QWidget):
    
    def __init__(self,client):
        super(AppWidget, self).__init__()
        self.client = client
        self.map = client.get_map()
        self.building_types = client.get_building_types()
        self.initUI()
        
    def initUI(self):
        self.buildDialog = BuildDialog(self,self.map.width,self.map.height,self.building_types)
        
        hbox = QtGui.QHBoxLayout()
        
        buildButton = QtGui.QPushButton('Build', self)
        buildButton.clicked.connect(self.show_build)
        
        refreshMapButton = QtGui.QPushButton('Refresh Map', self)
        refreshMapButton.clicked.connect(self.refreshMap)
        
        hbox.addWidget(buildButton)
        hbox.addWidget(refreshMapButton)
        
        vbox = QtGui.QVBoxLayout()
        self.mapWidget = MapWidget(self)
        vbox.addWidget(self.mapWidget)
        vbox.addLayout(hbox)
        self.setLayout(vbox)

        
        self.setGeometry(300, 300, 300, 150)
        self.setWindowTitle('City')
        self.show()
    
    def refreshMap(self):
        self.map = self.client.get_map()
        self.mapWidget.refresh()

    def show_build(self):
        self.buildDialog.show()
    
    def build(self):
        x,y,type = self.buildDialog.get_values()
        self.client.build(x,y,type)
        #self.refreshMap()
        
class MapWidget(QtGui.QWidget):
    def __init__(self,parent):
        super(MapWidget, self).__init__()
        self.parent = parent
        self.initUI()
    
    def initUI(self):
        self.grid = QtGui.QGridLayout()
        self.grid.setSpacing(1)
        width = self.parent.map.width
        height = self.parent.map.height
        
        for i, row in enumerate(self.parent.map.rows.MapFieldRow):
            for j, field in enumerate(row.fields.MapField):
            	if i > 0 and i < height-1 and j > 0 and j < width-1:
	                self.grid.addWidget(FieldWidget(field),i-1,width-j-1)
        self.setLayout(self.grid)
   
    def refresh(self):
        width = self.parent.map.width
        height = self.parent.map.height
        for i, row in enumerate(self.parent.map.rows.MapFieldRow):
            for j, field in enumerate(row.fields.MapField):
            	if i > 0 and i < height-1 and j > 0 and j < width-1:
	                self.grid.itemAtPosition(i-1,width-j-1).widget().refresh(field)
        super(MapWidget, self).repaint()
        
        
class FieldWidget(QtGui.QWidget):
   
    def __init__(self, field):
        super(FieldWidget, self).__init__()
        self.field = field
        self.initUI()
    
    def initUI(self):
        self.setMinimumSize(5, 5)
        self.setToolTip('<b>Coordinates:</b> %(coordinates)s<br/>\
                         <b>Height:</b> %(height)s<br/>\
                         <b>Tile Type:</b> %(tile_type)s<br/>\
                         <b>Zone Type:</b> %(zone_type)s<br/>' %
                         {'coordinates':'%s x %s' % (self.field.point.x, self.field.point.y),
                          'height':self.field.height,
                          'tile_type': self.field.tile_type,
                          'zone_type': self.field.zone_type
                          })
        
    def refresh(self,field):
        self.field = field
        self.initUI()
    
    def paintEvent(self, e):
        qp = QtGui.QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()
        
    def drawWidget(self, qp):
        size = self.size()
        w = size.width()
        h = size.height()
        if self.field.tile_type == "WATER":
            brush = QtGui.QColor(28, 94, 217)
        else:
            brush = QtGui.QColor(62, 196, 0)
        qp.setBrush(brush)
        qp.drawRect(0,0,w-1,h-1)
        
        
class BuildDialog(QtGui.QDialog):
    def __init__(self, parent=None,width=0,height=0,building_types = []):
        super(BuildDialog, self).__init__(parent)
        self.avaliable_x = [str(i) for i in range(1,width-1)]
        self.avaliable_y = [str(i) for i in range(1,height-1)]
        self.building_types = building_types
        self.initUI()
    
    def initUI(self):
        self.setWindowTitle('Build')
        self.point_x = QtGui.QComboBox(self)
        self.point_x.addItems(self.avaliable_x)
        self.l_point_x = QtGui.QLabel('X', self)
        self.point_y = QtGui.QComboBox(self)
        self.point_y.addItems(self.avaliable_y)
        self.l_point_y = QtGui.QLabel('Y', self)
        self.type = QtGui.QComboBox(self)
        self.type.addItems(self.building_types)
        self.l_type = QtGui.QLabel('Type', self)
        
        grid = QtGui.QGridLayout()
        grid.addWidget(self.l_point_x,0,0)
        grid.addWidget(self.point_x,0,1)
        grid.addWidget(self.l_point_y,1,0)
        grid.addWidget(self.point_y,1,1)
        grid.addWidget(self.l_type,2,0)
        grid.addWidget(self.type,2,1)
        
        hbox = QtGui.QHBoxLayout()
        closeButton = QtGui.QPushButton('Close',self)
        buildButton = QtGui.QPushButton('Build',self)
        closeButton.clicked.connect(self.close)
        buildButton.clicked.connect(self.build)
        hbox.addWidget(closeButton)
        hbox.addWidget(buildButton)
        
        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(grid)
        vbox.addLayout(hbox)
        self.setLayout(vbox)
        
    def get_values(self):
        return self.point_x.currentText(),self.point_y.currentText(),self.type.currentText()
    
    def build(self):
        self.parent().build()
        self.close()
        
    def close(self):
        self.point_x.setCurrentIndex(0)
        self.point_y.setCurrentIndex(0)
        self.type.setCurrentIndex(0)
        super(BuildDialog, self).close()
        
