'''
Module with web service
Using spyne library, manual: http://arskom.github.com/spyne/manual/index.html
'''
from server.model import Map, Point, MapField, MapFieldRow, \
    BuildingType, tile_types, zone_types, building_types, building_ids
from spyne.decorator import srpc
from spyne.service import ServiceBase
from spyne.model.primitive import Boolean

class OpenCityService(ServiceBase):
    connector = None
    
    # this method is horrible, but it is only for PS...
    @srpc(_returns=Map)
    def get_map():
        map = OpenCityService.connector.get_map()
        # convert map and return
        returnMap = Map()
        returnMap.height = map.height
        returnMap.width = map.width
        returnMap.rows = []
        for row in map.rows:
            mRow = MapFieldRow()
            mRow.fields = []
            for field in row:
                mField = MapField()
                fieldPoint = Point()
                fieldPoint.x = field.point.x
                fieldPoint.y = field.point.y
                mField.point = fieldPoint
                mField.height = field.height
                mField.tile_type = tile_types[field.tileType]
                mField.zone_type = zone_types[field.zoneType]
                mRow.fields.append(mField)
            returnMap.rows.append(mRow)
        return returnMap
    
    @srpc(Point, BuildingType, _returns=Boolean)
    def build(point, building_type):
        bid = building_ids[building_types.index(str(building_type))]
        print point, point.x, point.y, building_type, bid
        return OpenCityService.connector.build(point,bid)
