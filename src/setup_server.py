from server.service import OpenCityService
from server.city_connector.sock import SocketConnector
from server.city_connector.test import TestConnector
from spyne.util.simple import wsgi_soap_application
import logging
import sys
import getopt

if __name__ == "__main__":
	# get address from command line options
	address_ip = '127.0.0.1'
	address_port = 7989
	address = "http://%s:%s" % (address_ip,address_port)
	
	game_host = "localhost"
	game_port = 10002
	
	try:
		opts = getopt.getopt(sys.argv[1:], "t")
		test = ("-t", "") in opts[0]
	except getopt.GetoptError:
		print "Launch: python ", sys.argv[0], "[-t]"
		sys.exit()
	
	if test:
		OpenCityService.connector = TestConnector(20,20)
	else:
		OpenCityService.connector = SocketConnector(game_host, game_port)
	
	from wsgiref.simple_server import make_server

	logging.basicConfig(level=logging.DEBUG)
	logging.getLogger('spyne.protocol.xml').setLevel(logging.DEBUG)

	logging.info("listening to %s" % address)
	logging.info("wsdl is at: %s?wsdl" % address)

	wsgi_app = wsgi_soap_application([OpenCityService], 'open-city.soap')
	server = make_server(address_ip, address_port, wsgi_app)
	server.serve_forever()
