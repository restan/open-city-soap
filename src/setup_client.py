
from PySide import QtGui
from client.client import ClientProxy
from client.client_gui import AppWidget
import sys

if __name__ == "__main__":
    # get address from command line
    if len(sys.argv) < 2:
        address = 'localhost:7989'
    else:
        address = sys.argv[1]
    wsdl_address = 'http://' + address + '/?wsdl'
    client = ClientProxy(wsdl_address)
    
    # start gui
    app = QtGui.QApplication(sys.argv)
    app_widget = AppWidget(client)
    sys.exit(app.exec_())
