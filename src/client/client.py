'''
Module for communication with web service
Using suds lib, manual: https://fedorahosted.org/suds/wiki/Documentation
'''

from suds.client import Client

class ClientProxy(object):
    def __init__(self,address):
        self.client = Client(address)
    
    def get_map(self):
        return self.client.service.get_map()
    
    def build(self,x,y,building_type):
        point = self.client.factory.create('Point')
        point.x = x
        point.y = y
        bt = self.client.factory.create('BuildingType')[building_type]
        return self.client.service.build(point,bt)

    def get_building_types(self):
        return [bt[0] for bt in self.client.factory.create('BuildingType')]
