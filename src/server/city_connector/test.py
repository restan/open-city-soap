'''
Created on 05-11-2012

@author: gregor
'''
from server.city_connector.model import Map, Point
from server.model import tile_types, zone_types
import random

class TestConnector(object):
    def __init__(self,map_width,map_height):
        self.city = TestCity(map_width,map_height)
        
    # this method will pull map information from the game in real connector
    def get_map(self):
        return self.city.getMap()
    
    # this method will build a building in real connectoru
    def build(self,point,buildingType):
        return self.city.build(Point(point.x, point.y), buildingType)
    

class TestCity(object):
    def __init__(self,map_width,map_height):
        self.map = Map(map_width,map_height)
        # initialize map with random values - in real connector won't take place
        for row in self.map.rows:
            for field in row:
                field.height = random.randint(-3, 3)
                field.tileType = random.randint(0, len(tile_types)-1)
                field.zoneType = random.randint(0, len(zone_types)-1)
        
    def getMap(self):
        return self.map
        
    def build(self, point, tile_type):
        self.map.rows[point.y][point.x].tileType = tile_types.index("HOUSE")
        return True

